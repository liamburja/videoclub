<?php

namespace VideoclubBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;


class ProductoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('titulo', TextType::class)
        ->add('tituloOriginal', TextType::class)
        ->add('anyo', NumberType::class)
        ->add('sinopsis', TextareaType::class, array(
                'attr' => array(
                    'cols' => 30,
                    'rows' => 10,
                    'placeholder' => 'Sinopsis que deseas introducir'
                )
            ))
        ->add('portada', TextType::class, array('label' => 'Portada (Debera meter URL): '))
        ->add('edadRecomendada', NumberType::class, array('label' => 'edad Recomendada: ' ))
        ->add('esJuego', ChoiceType::class, array('label' => '¿Es juego?',
          'choices' => array(
            'No' => 0,
            'Si' => 1,
          )
        ))
        ->add('duracion', NumberType::class)
        ->add('director', TextType::class)
        ->add('reparto', TextType::class)
        ->add('desarrolladora', TextType::class)
        ->add('multijugador', ChoiceType::class, array('label' => '¿Multijugador?',
          'choices' => array(
            'No' => 0,
            'Si' => 1,
          )
        ))
        ->add('idgenero', EntityType::class, array('label' => 'Genero',
          'class' => 'VideoclubBundle:Genero',
          'choice_label' => 'tipo',
        ))
        ->add('ididioma', EntityType::class, array('label' => 'Idioma',
        'class' => 'VideoclubBundle:Idioma',
        'choice_label' => 'tipo',
        ))
         ->add('idpais', EntityType::class, array('label' => 'Pais',
           'class' => 'VideoclubBundle:Pais',
           'choice_label' => 'nombre',
         ))
        ->add('Guardar', SubmitType::class, array('label' => 'Insertar'))
        ->add('Borrar', ResetType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
      $collectionConstraint = new Collection(array(
            'titulo' => array(
                new NotBlank(array('message' => 'El nombre no puede estar vacío.')),
                new Length(array('min' => 3))
            ),
            'tituloOriginal' => array(
                new NotBlank(array('message' => 'El titulo origional no puede estar vacío.')),
                new Length(array('min' => 3))
            ),
            'anyo' => array(
                new NotBlank(array('message' => 'El motivo no puede estar vacío.')),
                new Length(array('min' => 3))
            ),
            'sinopsis  ' => array(
                new NotBlank(array('message' => 'La sinopsis no puede estar vacía.')),
                new Length(array('min' => 5))
            )
        ));

    #    $resolver->setDefaults(array(
    #        'constraints' => $collectionConstraint
    #    ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'videoclubBundle_producto';
    }


}
