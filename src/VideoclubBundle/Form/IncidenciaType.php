<?php

namespace VideoclubBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class IncidenciaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('fechaIncidencia', DateType::class)
        ->add('fechaResulta', DateType::class)
        ->add('observaciones', TextType::class, array('label' => 'Observaciones:'))
        ->add('iditem', EntityType::class, array('label' => 'Item',
        'class' => 'VideoclubBundle:Item',
        'choice_label' => 'iditem',
        ))
         ->add('idTipo_incidencia', EntityType::class, array('label' => 'Tipo Incidencia',
         'class' => 'VideoclubBundle:TipoIncidencia',
         'choice_label' => 'nombre',
         ))
        ->add('Guardar', SubmitType::class, array('label' => 'Insertar'))
        ->add('Borrar', ResetType::class);
    }

    /**
     * {@inheritdoc}
     */
    // public function configureOptions(OptionsResolver $resolver)
    // {
    //   $collectionConstraint = new Collection(array(
    //         'fecha_incidencia' => array(
    //             new NotBlank(array('message' => 'La fecha de alquiler no puede estar vacío.')),
    //             new Length(array('min' => 3))
    //         ),
    //         'iditem' => array(
    //             new NotBlank(array('message' => 'El item no puede estar vacío.')),
    //             new Length(array('min' => 3))
    //         ),
    //         'idTipo_incidencia' => array(
    //             new NotBlank(array('message' => 'El cliente no puede estar vacío.')),
    //             new Length(array('min' => 5))
    //         ),
    //         'observaciones' => array(
    //             new NotBlank(array('message' => 'El tipo de alquiler no puede estar vacío.')),
    //             new Length(array('min' => 3))
    //         ),
    //     ));
    //
    //     $resolver->setDefaults(array(
    //         'constraints' => $collectionConstraint
    //     ));
    // }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'videoclubBundle_incidencia';
    }


}
