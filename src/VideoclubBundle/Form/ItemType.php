<?php

namespace VideoclubBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class ItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('idestado', EntityType::class, array('label' => 'Estado',
        'class' => 'VideoclubBundle:Estado',
        'choice_label' => 'tipo',
        ))
        ->add('idformato', EntityType::class, array('label' => 'Formato',
        'class' => 'VideoclubBundle:Formato',
        'choice_label' => 'tipo',
        ))
        ->add('idproducto', EntityType::class, array('label' => 'Producto',
        'class' => 'VideoclubBundle:Producto',
        'choice_label' => 'titulo',
        ))
        ->add('idtipoalquiler', EntityType::class, array('label' => 'Tipo alquiler',
        'class' => 'VideoclubBundle:TipoAlquiler',
        'choice_label' => 'nombre',
        ))
        ->add('Guardar', SubmitType::class, array('label' => 'Insertar'))
        ->add('Borrar', ResetType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'videoclubBundle_producto';
    }


}
