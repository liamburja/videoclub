<?php

namespace VideoclubBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
class ClienteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', TextType::class)
        ->add('apellido1', TextType::class)
        ->add('apellido2', TextType::class)
        ->add('dniNie', TextType::class)
        ->add('direccion', TextType::class)
        ->add('email', EmailType::class)
        ->add('telefono1', NumberType::class)
        ->add('telefono2', NumberType::class)
        ->add('fechaNacimiento', DateType::class, array(
          'widget' => 'choice',
          'years' => range(1960,2017),
        ))
        ->add('fechaRegistro', DateType::class)
        ->add('username', TextType::class)
        ->add('plainpassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Contraseña:'),
                'second_options' => array('label' => 'Repita contraseña:'),
            ))
         ->add('idpais', EntityType::class, array( 'label'=> 'Pais',
         'class' => 'VideoclubBundle:Pais',
         'choice_label' => 'nombre',
         ))
        ->add('Guardar', SubmitType::class)
        ->add('Borrar', ResetType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VideoclubBundle\Entity\Cliente'
        ));
    }



    /**
     * {@inheritdoc}
     */
    // public function getBlockPrefix()
    // {
    //     return 'videoclubBundle_cliente';
    // }
    // public function buildForm(FormBuilderInterface $builder, array $options)
    // {
    //     $builder->add('name');
    // }
    //
    // public function getParent()
    // {
    //     return 'FOS\UserBundle\Form\Type\ClienteType';
    //
    //     // Or for Symfony < 2.8
    //     // return 'fos_user_registration';
    // }
    //
    // // For Symfony 2.x
    // public function getName()
    // {
    //     return $this->getBlockPrefix();
    // }


}
