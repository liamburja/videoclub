<?php

namespace VideoclubBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReservaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('fecha_reserva', DateType::class)
        ->add('iditem', EntityType::class, array('label' => 'Item',
        'class' => 'VideoclubBundle:Item',
        'choice_label' => 'iditem',
        ))
        ->add('idcliente', EntityType::class, array('label' => 'Cliente',
        'class' => 'VideoclubBundle:Cliente',
        'choice_label' => 'nombre',
        ))
        ->add('Guardar', SubmitType::class, array('label' => 'Insertar'))
        ->add('Borrar', ResetType::class);
    }

    /**
     * {@inheritdoc}
     */
    // public function configureOptions(OptionsResolver $resolver)
    // {
    //   $collectionConstraint = new Collection(array(
    //         'fecha_reserva' => array(
    //             new NotBlank(array('message' => 'La fecha de la reserva no puede estar vacío.')),
    //             new Length(array('min' => 3))
    //         ),
    //         'iditem' => array(
    //             new NotBlank(array('message' => 'El item no puede estar vacío.'))
    //         ),
    //         'idcliente  ' => array(
    //             new NotBlank(array('message' => 'El cliente no puede estar vacío.'))
    //         ),
    //     ));
    //
    //     $resolver->setDefaults(array(
    //         'constraints' => $collectionConstraint
    //     ));
    // }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'videoclubBundle_reserva';
    }


}
