<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;
/**
 * Cliente
 *
 * @ORM\Table(name="cliente", indexes={@ORM\Index(name="fk_Cliente_Poblacion1_idx", columns={"IdPoblacion"}), @ORM\Index(name="fk_Cliente_Pais1_idx", columns={"IdPais"}), @ORM\Index(name="IdRol_idx", columns={"IdRol"})})
 * @ORM\Entity
 */
class Cliente extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdCliente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=25, nullable=true)
     */
    protected $nombre;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="Apellido1", type="string", length=30, nullable=true)
     */
    protected $apellido1;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="Apellido2", type="string", length=30, nullable=true)
     */
    protected $apellido2;

    /**
     * @var string
     *
     * @ORM\Column(name="DNI_NIE", type="string", length=20, nullable=true)
     */
    protected $dniNie;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="Direccion", type="string", length=100, nullable=true)
     */
    protected $direccion;


    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="Telefono1", type="string", length=20, nullable=true)
     */
    protected $telefono1;

    /**
     * @var string
     *
     * @ORM\Column(name="Telefono2", type="string", length=20, nullable=true)
     */
    protected $telefono2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Nacimiento", type="date", nullable=true)
     */
    protected $fechaNacimiento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Registro", type="date", nullable=true)
     */
    protected $fechaRegistro;


    /**
     * @var integer
     * @ORM\Column(name="IdPoblacion", type="integer", nullable=true)
     */
    protected $idpoblacion;

    /**
     * @var \Rol
     *
     * @ORM\ManyToOne(targetEntity="Rol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdRol", referencedColumnName="IdRol", onDelete="CASCADE")
     * })
     */
    protected $idrol;

    /**
     * @var \Pais
     *
     * @ORM\ManyToOne(targetEntity="Pais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdPais", referencedColumnName="IdPais", onDelete="CASCADE")
     * })
     */
    protected $idpais;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1
     *
     * @param string $apellido1
     *
     * @return Cliente
     */
    public function setApellido1($apellido1)
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    /**
     * Get apellido1
     *
     * @return string
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2
     *
     * @param string $apellido2
     *
     * @return Cliente
     */
    public function setApellido2($apellido2)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2
     *
     * @return string
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set dniNie
     *
     * @param string $dniNie
     *
     * @return Cliente
     */
    public function setDniNie($dniNie)
    {
        $this->dniNie = $dniNie;

        return $this;
    }

    /**
     * Get dniNie
     *
     * @return string
     */
    public function getDniNie()
    {
        return $this->dniNie;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Cliente
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }


    /**
     * Set telefono1
     *
     * @param string $telefono1
     *
     * @return Cliente
     */
    public function setTelefono1($telefono1)
    {
        $this->telefono1 = $telefono1;

        return $this;
    }

    /**
     * Get telefono1
     *
     * @return string
     */
    public function getTelefono1()
    {
        return $this->telefono1;
    }

    /**
     * Set telefono2
     *
     * @param string $telefono2
     *
     * @return Cliente
     */
    public function setTelefono2($telefono2)
    {
        $this->telefono2 = $telefono2;

        return $this;
    }

    /**
     * Get telefono2
     *
     * @return string
     */
    public function getTelefono2()
    {
        return $this->telefono2;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     *
     * @return Cliente
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set fechaRegistro
     *
     * @param \DateTime $fechaRegistro
     *
     * @return Cliente
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    /**
     * Get fechaRegistro
     *
     * @return \DateTime
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }


    /**
     * Set password
     *
     * @param string $password
     *
     * @return Cliente
     */


    /**
     * Set idpoblacion
     *
     * @param integer $idpoblacion
     *
     * @return Cliente
     */
    public function setIdpoblacion($idpoblacion)
    {
        $this->idpoblacion = $idpoblacion;

        return $this;
    }

    /**
     * Get idpoblacion
     *
     * @return integer
     */
    public function getIdpoblacion()
    {
        return $this->idpoblacion;
    }

    /**
     * Set idrol
     *
     * @param \VideoclubBundle\Entity\Rol $idrol
     *
     * @return Cliente
     */
    public function setIdrol(\VideoclubBundle\Entity\Rol $idrol = null)
    {
        $this->idrol = $idrol;

        return $this;
    }

    /**
     * Get idrol
     *
     * @return \VideoclubBundle\Entity\Rol
     */
    public function getIdrol()
    {
        return $this->idrol;
    }

    /**
     * Set idpais
     *
     * @param \VideoclubBundle\Entity\Pais $idpais
     *
     * @return Cliente
     */
    public function setIdpais(\VideoclubBundle\Entity\Pais $idpais = null)
    {
        $this->idpais = $idpais;

        return $this;
    }

    /**
     * Get idpais
     *
     * @return \VideoclubBundle\Entity\Pais
     */
    public function getIdpais()
    {
        return $this->idpais;
    }



    public function getRoles()
    {
      return array('ROLE_USER');
    }

    public function isAdmin()
    {
      return $this->getIdrol()->idrol == 1;
    }

    public function __construct()
   {
       parent::__construct();

   }
}
