<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comunidad
 *
 * @ORM\Table(name="comunidad", indexes={@ORM\Index(name="fk_Comunidad_Pais1_idx", columns={"IdPais"})})
 * @ORM\Entity
 */
class Comunidad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdComunidad", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcomunidad;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var \Pais
     *
     * @ORM\ManyToOne(targetEntity="Pais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdPais", referencedColumnName="IdPais")
     * })
     */
    private $idpais;



    /**
     * Get idcomunidad
     *
     * @return integer
     */
    public function getIdcomunidad()
    {
        return $this->idcomunidad;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Comunidad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set idpais
     *
     * @param \VideoclubBundle\Entity\Pais $idpais
     *
     * @return Comunidad
     */
    public function setIdpais(\VideoclubBundle\Entity\Pais $idpais = null)
    {
        $this->idpais = $idpais;

        return $this;
    }

    /**
     * Get idpais
     *
     * @return \VideoclubBundle\Entity\Pais
     */
    public function getIdpais()
    {
        return $this->idpais;
    }
}
