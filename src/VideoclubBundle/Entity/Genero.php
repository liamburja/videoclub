<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Genero
 *
 * @ORM\Table(name="genero")
 * @ORM\Entity
 */
class Genero
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdGenero", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgenero;

    /**
     * @var string
     *
     * @ORM\Column(name="Tipo", type="string", length=45, nullable=true)
     */
    private $tipo;



    /**
     * Get idgenero
     *
     * @return integer
     */
    public function getIdgenero()
    {
        return $this->idgenero;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Genero
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
