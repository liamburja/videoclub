<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formato
 *
 * @ORM\Table(name="formato")
 * @ORM\Entity
 */
class Formato
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdFormato", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idformato;

    /**
     * @var string
     *
     * @ORM\Column(name="Tipo", type="string", length=45, nullable=true)
     */
    private $tipo;



    /**
     * Get idformato
     *
     * @return integer
     */
    public function getIdformato()
    {
        return $this->idformato;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Formato
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
