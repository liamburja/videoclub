<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incidencia
 *
 * @ORM\Table(name="incidencia", indexes={@ORM\Index(name="fk_Item_idx", columns={"IdItem"}), @ORM\Index(name="fk_TipoIncidencia_idx", columns={"IdTipoIncidencia"})})
 * @ORM\Entity
 */
class Incidencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdIncidencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idincidencia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Incidencia", type="datetime", nullable=true)
     */
    private $fechaIncidencia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Resulta", type="datetime", nullable=true)
     */
    private $fechaResulta;

    /**
     * @var string
     *
     * @ORM\Column(name="Observaciones", type="string", length=500, nullable=true)
     */
    private $observaciones;

    /**
     * @var \Item
     *
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdItem", referencedColumnName="IdItem", onDelete="CASCADE")
     * })
     */
    private $iditem;

    /**
     * @var \TipoIncidencia
     *
     * @ORM\ManyToOne(targetEntity="TipoIncidencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdTipoIncidencia", referencedColumnName="IdTipoIncidencia", onDelete="CASCADE")
     * })
     */
    private $idtipoincidencia;



    /**
     * Get idincidencia
     *
     * @return integer
     */
    public function getIdincidencia()
    {
        return $this->idincidencia;
    }

    /**
     * Set fechaIncidencia
     *
     * @param \DateTime $fechaIncidencia
     *
     * @return Incidencia
     */
    public function setFechaIncidencia($fechaIncidencia)
    {
        $this->fechaIncidencia = $fechaIncidencia;

        return $this;
    }

    /**
     * Get fechaIncidencia
     *
     * @return \DateTime
     */
    public function getFechaIncidencia()
    {
        return $this->fechaIncidencia;
    }

    /**
     * Set fechaResulta
     *
     * @param \DateTime $fechaResulta
     *
     * @return Incidencia
     */
    public function setFechaResulta($fechaResulta)
    {
        $this->fechaResulta = $fechaResulta;

        return $this;
    }

    /**
     * Get fechaResulta
     *
     * @return \DateTime
     */
    public function getFechaResulta()
    {
        return $this->fechaResulta;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     *
     * @return Incidencia
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set iditem
     *
     * @param \VideoclubBundle\Entity\Item $iditem
     *
     * @return Incidencia
     */
    public function setIditem(\VideoclubBundle\Entity\Item $iditem = null)
    {
        $this->iditem = $iditem;

        return $this;
    }

    /**
     * Get iditem
     *
     * @return \VideoclubBundle\Entity\Item
     */
    public function getIditem()
    {
        return $this->iditem;
    }

    /**
     * Set idtipoincidencia
     *
     * @param \VideoclubBundle\Entity\TipoIncidencia $idtipoincidencia
     *
     * @return Incidencia
     */
    public function setIdtipoincidencia(\VideoclubBundle\Entity\TipoIncidencia $idtipoincidencia = null)
    {
        $this->idtipoincidencia = $idtipoincidencia;

        return $this;
    }

    /**
     * Get idtipoincidencia
     *
     * @return \VideoclubBundle\Entity\TipoIncidencia
     */
    public function getIdtipoincidencia()
    {
        return $this->idtipoincidencia;
    }
}
