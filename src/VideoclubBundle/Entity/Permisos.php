<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permisos
 *
 * @ORM\Table(name="permisos")
 * @ORM\Entity
 */
class Permisos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdPermisos", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpermisos;

    /**
     * @var string
     *
     * @ORM\Column(name="Tipo", type="string", length=45, nullable=true)
     */
    private $tipo;



    /**
     * Get idpermisos
     *
     * @return integer
     */
    public function getIdpermisos()
    {
        return $this->idpermisos;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Permisos
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
