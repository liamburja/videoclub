<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Producto
 *
 * @ORM\Table(name="producto", indexes={@ORM\Index(name="fk_Producto_Pais1_idx", columns={"IdPais"}), @ORM\Index(name="Idgenero", columns={"Idgenero"}), @ORM\Index(name="IdIdioma", columns={"IdIdioma"})})
 * @ORM\Entity
 */
class Producto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdProducto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idproducto;

    /**
     * @var string
     *
     * @ORM\Column(name="Titulo", type="string", length=45, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="Titulo_Original", type="string", length=100, nullable=true)
     */
    private $tituloOriginal;

    /**
     * @var string
     *
     * @ORM\Column(name="Anyo", type="string", length=4, nullable=true)
     */
    private $anyo;

    /**
     * @var string
     *
     * @ORM\Column(name="Sinopsis", type="text", length=65535, nullable=true)
     */
    private $sinopsis;

    /**
     * @var string
     *
     * @ORM\Column(name="Portada", type="string", length=255, nullable=true)
     */
    private $portada;

    /**
     * @var string
     *
     * @ORM\Column(name="Edad_Recomendada", type="string", length=2, nullable=true)
     */
    private $edadRecomendada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Es_Juego", type="boolean", nullable=true)
     */
    private $esJuego;

    /**
     * @var integer
     *
     * @ORM\Column(name="Duracion", type="integer", nullable=true)
     */
    private $duracion;

    /**
     * @var string
     *
     * @ORM\Column(name="Director", type="string", length=45, nullable=true)
     */
    private $director;

    /**
     * @var string
     *
     * @ORM\Column(name="Reparto", type="text", length=65535, nullable=true)
     */
    private $reparto;

    /**
     * @var string
     *
     * @ORM\Column(name="Desarrolladora", type="string", length=150, nullable=true)
     */
    private $desarrolladora;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Multijugador", type="boolean", nullable=true)
     */
    private $multijugador;

    /**
     * @var \Genero
     *
     * @ORM\ManyToOne(targetEntity="Genero")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Idgenero", referencedColumnName="IdGenero", onDelete="CASCADE")
     * })
     */
    private $idgenero;

    /**
     * @var \Idioma
     *
     * @ORM\ManyToOne(targetEntity="Idioma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdIdioma", referencedColumnName="IdIdioma", onDelete="CASCADE")
     * })
     */
    private $ididioma;

    /**
     * @var \Pais
     *
     * @ORM\ManyToOne(targetEntity="Pais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdPais", referencedColumnName="IdPais", onDelete="CASCADE")
     * })
     */
    private $idpais;



    /**
     * Get idproducto
     *
     * @return integer
     */
    public function getIdproducto()
    {
        return $this->idproducto;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Producto
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set tituloOriginal
     *
     * @param string $tituloOriginal
     *
     * @return Producto
     */
    public function setTituloOriginal($tituloOriginal)
    {
        $this->tituloOriginal = $tituloOriginal;

        return $this;
    }

    /**
     * Get tituloOriginal
     *
     * @return string
     */
    public function getTituloOriginal()
    {
        return $this->tituloOriginal;
    }

    /**
     * Set anyo
     *
     * @param string $anyo
     *
     * @return Producto
     */
    public function setAnyo($anyo)
    {
        $this->anyo = $anyo;

        return $this;
    }

    /**
     * Get anyo
     *
     * @return string
     */
    public function getAnyo()
    {
        return $this->anyo;
    }

    /**
     * Set sinopsis
     *
     * @param string $sinopsis
     *
     * @return Producto
     */
    public function setSinopsis($sinopsis)
    {
        $this->sinopsis = $sinopsis;

        return $this;
    }

    /**
     * Get sinopsis
     *
     * @return string
     */
    public function getSinopsis()
    {
        return $this->sinopsis;
    }

    /**
     * Set portada
     *
     * @param string $portada
     *
     * @return Producto
     */
    public function setPortada($portada)
    {
        $this->portada = $portada;

        return $this;
    }

    /**
     * Get portada
     *
     * @return string
     */
    public function getPortada()
    {
        return $this->portada;
    }

    /**
     * Set edadRecomendada
     *
     * @param string $edadRecomendada
     *
     * @return Producto
     */
    public function setEdadRecomendada($edadRecomendada)
    {
        $this->edadRecomendada = $edadRecomendada;

        return $this;
    }

    /**
     * Get edadRecomendada
     *
     * @return string
     */
    public function getEdadRecomendada()
    {
        return $this->edadRecomendada;
    }

    /**
     * Set esJuego
     *
     * @param boolean $esJuego
     *
     * @return Producto
     */
    public function setEsJuego($esJuego)
    {
        $this->esJuego = $esJuego;

        return $this;
    }

    /**
     * Get esJuego
     *
     * @return boolean
     */
    public function getEsJuego()
    {
        return $this->esJuego;
    }

    /**
     * Set duracion
     *
     * @param integer $duracion
     *
     * @return Producto
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return integer
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set director
     *
     * @param string $director
     *
     * @return Producto
     */
    public function setDirector($director)
    {
        $this->director = $director;

        return $this;
    }

    /**
     * Get director
     *
     * @return string
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * Set reparto
     *
     * @param string $reparto
     *
     * @return Producto
     */
    public function setReparto($reparto)
    {
        $this->reparto = $reparto;

        return $this;
    }

    /**
     * Get reparto
     *
     * @return string
     */
    public function getReparto()
    {
        return $this->reparto;
    }

    /**
     * Set desarrolladora
     *
     * @param string $desarrolladora
     *
     * @return Producto
     */
    public function setDesarrolladora($desarrolladora)
    {
        $this->desarrolladora = $desarrolladora;

        return $this;
    }

    /**
     * Get desarrolladora
     *
     * @return string
     */
    public function getDesarrolladora()
    {
        return $this->desarrolladora;
    }

    /**
     * Set multijugador
     *
     * @param boolean $multijugador
     *
     * @return Producto
     */
    public function setMultijugador($multijugador)
    {
        $this->multijugador = $multijugador;

        return $this;
    }

    /**
     * Get multijugador
     *
     * @return boolean
     */
    public function getMultijugador()
    {
        return $this->multijugador;
    }

    /**
     * Set idgenero
     *
     * @param \VideoclubBundle\Entity\Genero $idgenero
     *
     * @return Producto
     */
    public function setIdgenero(\VideoclubBundle\Entity\Genero $idgenero = null)
    {
        $this->idgenero = $idgenero;

        return $this;
    }

    /**
     * Get idgenero
     *
     * @return \VideoclubBundle\Entity\Genero
     */
    public function getIdgenero()
    {
        return $this->idgenero;
    }

    /**
     * Set ididioma
     *
     * @param \VideoclubBundle\Entity\Idioma $ididioma
     *
     * @return Producto
     */
    public function setIdidioma(\VideoclubBundle\Entity\Idioma $ididioma = null)
    {
        $this->ididioma = $ididioma;

        return $this;
    }

    /**
     * Get ididioma
     *
     * @return \VideoclubBundle\Entity\Idioma
     */
    public function getIdidioma()
    {
        return $this->ididioma;
    }

    /**
     * Set idpais
     *
     * @param \VideoclubBundle\Entity\Pais $idpais
     *
     * @return Producto
     */
    public function setIdpais(\VideoclubBundle\Entity\Pais $idpais = null)
    {
        $this->idpais = $idpais;

        return $this;
    }

    /**
     * Get idpais
     *
     * @return \VideoclubBundle\Entity\Pais
     */
    public function getIdpais()
    {
        return $this->idpais;
    }
}
