<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alquiler
 *
 * @ORM\Table(name="alquiler", indexes={@ORM\Index(name="IdItem_idx", columns={"IdItem"}), @ORM\Index(name="IdCliente_idx", columns={"IdCliente"}), @ORM\Index(name="IdTipo_Alquiler_idx", columns={"IdTipo_Alquiler"})})
 * @ORM\Entity
 */
class Alquiler
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdAlquiler", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idalquiler;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Alquiler", type="datetime", nullable=true)
     */
    private $fechaAlquiler;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Devolucion", type="datetime", nullable=true)
     */
    private $fechaDevolucion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Devuelto", type="datetime", nullable=true)
     */
    private $fechaDevuelto;

    /**
     * @var \Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdCliente", referencedColumnName="IdCliente", onDelete="CASCADE")
     * })
     */
    private $idcliente;

    /**
     * @var \Item
     *
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdItem", referencedColumnName="IdItem", onDelete="CASCADE")
     * })
     */
    private $iditem;

    /**
     * @var \TipoAlquiler
     *
     * @ORM\ManyToOne(targetEntity="TipoAlquiler")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdTipo_Alquiler", referencedColumnName="IdTipoAlquiler", onDelete="CASCADE")
     * })
     */
    private $idtipoAlquiler;



    /**
     * Get idalquiler
     *
     * @return integer
     */
    public function getIdalquiler()
    {
        return $this->idalquiler;
    }

    /**
     * Set fechaAlquiler
     *
     * @param \DateTime $fechaAlquiler
     *
     * @return Alquiler
     */
    public function setFechaAlquiler($fechaAlquiler)
    {
        $this->fechaAlquiler = $fechaAlquiler;

        return $this;
    }

    /**
     * Get fechaAlquiler
     *
     * @return \DateTime
     */
    public function getFechaAlquiler()
    {
        return $this->fechaAlquiler;
    }

    /**
     * Set fechaDevolucion
     *
     * @param \DateTime $fechaDevolucion
     *
     * @return Alquiler
     */
    public function setFechaDevolucion($fechaDevolucion)
    {
        $this->fechaDevolucion = $fechaDevolucion;

        return $this;
    }

    /**
     * Get fechaDevolucion
     *
     * @return \DateTime
     */
    public function getFechaDevolucion()
    {
        return $this->fechaDevolucion;
    }

    /**
     * Set fechaDevuelto
     *
     * @param \DateTime $fechaDevuelto
     *
     * @return Alquiler
     */
    public function setFechaDevuelto($fechaDevuelto)
    {
        $this->fechaDevuelto = $fechaDevuelto;

        return $this;
    }

    /**
     * Get fechaDevuelto
     *
     * @return \DateTime
     */
    public function getFechaDevuelto()
    {
        return $this->fechaDevuelto;
    }

    /**
     * Set idcliente
     *
     * @param \VideoclubBundle\Entity\Cliente $idcliente
     *
     * @return Alquiler
     */
    public function setIdcliente(\VideoclubBundle\Entity\Cliente $idcliente = null)
    {
        $this->idcliente = $idcliente;

        return $this;
    }

    /**
     * Get idcliente
     *
     * @return \VideoclubBundle\Entity\Cliente
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }

    /**
     * Set iditem
     *
     * @param \VideoclubBundle\Entity\Item $iditem
     *
     * @return Alquiler
     */
    public function setIditem(\VideoclubBundle\Entity\Item $iditem = null)
    {
        $this->iditem = $iditem;

        return $this;
    }

    /**
     * Get iditem
     *
     * @return \VideoclubBundle\Entity\Item
     */
    public function getIditem()
    {
        return $this->iditem;
    }

    /**
     * Set idtipoAlquiler
     *
     * @param \VideoclubBundle\Entity\TipoAlquiler $idtipoAlquiler
     *
     * @return Alquiler
     */
    public function setIdtipoAlquiler(\VideoclubBundle\Entity\TipoAlquiler $idtipoAlquiler = null)
    {
        $this->idtipoAlquiler = $idtipoAlquiler;

        return $this;
    }

    /**
     * Get idtipoAlquiler
     *
     * @return \VideoclubBundle\Entity\TipoAlquiler
     */
    public function getIdtipoAlquiler()
    {
        return $this->idtipoAlquiler;
    }
}
