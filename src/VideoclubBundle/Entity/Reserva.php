<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reserva
 *
 * @ORM\Table(name="reserva", indexes={@ORM\Index(name="IdItem_idx", columns={"IdItem"}), @ORM\Index(name="IdCliente_idx", columns={"IdCliente"})})
 * @ORM\Entity
 */
class Reserva
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdReserva", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idreserva;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_Reserva", type="datetime", nullable=true)
     */
    private $fechaReserva;

    /**
     * @var \Item
     *
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdItem", referencedColumnName="IdItem", onDelete="CASCADE")
     * })
     */
    private $iditem;

    /**
     * @var \Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdCliente", referencedColumnName="IdCliente", onDelete="CASCADE")
     * })
     */
    private $idcliente;



    /**
     * Get idreserva
     *
     * @return integer
     */
    public function getIdreserva()
    {
        return $this->idreserva;
    }

    /**
     * Set fechaReserva
     *
     * @param \DateTime $fechaReserva
     *
     * @return Reserva
     */
    public function setFechaReserva($fechaReserva)
    {
        $this->fechaReserva = $fechaReserva;

        return $this;
    }

    /**
     * Get fechaReserva
     *
     * @return \DateTime
     */
    public function getFechaReserva()
    {
        return $this->fechaReserva;
    }

    /**
     * Set iditem
     *
     * @param integer $iditem
     *
     * @return Reserva
     */
    public function setIditem($iditem)
    {
        $this->iditem = $iditem;

        return $this;
    }

    /**
     * Get iditem
     *
     * @return integer
     */
    public function getIditem()
    {
        return $this->iditem;
    }

    /**
     * Set idcliente
     *
     * @param integer $idcliente
     *
     * @return Reserva
     */
    public function setIdcliente($idcliente)
    {
        $this->idcliente = $idcliente;

        return $this;
    }

    /**
     * Get idcliente
     *
     * @return integer
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }
}
