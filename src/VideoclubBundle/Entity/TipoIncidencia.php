<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoIncidencia
 *
 * @ORM\Table(name="tipo_incidencia")
 * @ORM\Entity
 */
class TipoIncidencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdTipoIncidencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtipoincidencia;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="Descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;



    /**
     * Get idtipoincidencia
     *
     * @return integer
     */
    public function getIdtipoincidencia()
    {
        return $this->idtipoincidencia;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoIncidencia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return TipoIncidencia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
