<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item", indexes={@ORM\Index(name="fk_Producto_idx", columns={"IdProducto"}), @ORM\Index(name="fk_Formato_idx", columns={"IdFormato"}), @ORM\Index(name="fk_Estado_idx", columns={"IdEstado"}), @ORM\Index(name="fk_TipoAlquiler_idx", columns={"IdTipoAlquiler"})})
 * @ORM\Entity
 */
class Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdItem", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iditem;

    /**
     * @var \Estado
     *
     * @ORM\ManyToOne(targetEntity="Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdEstado", referencedColumnName="IdEstado", onDelete="CASCADE")
     * })
     */
    private $idestado;

    /**
     * @var \Formato
     *
     * @ORM\ManyToOne(targetEntity="Formato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdFormato", referencedColumnName="IdFormato", onDelete="CASCADE")
     * })
     */
    private $idformato;

    /**
     * @var \Producto
     *
     * @ORM\ManyToOne(targetEntity="Producto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdProducto", referencedColumnName="IdProducto", onDelete="CASCADE")
     * })
     */
    private $idproducto;

    /**
     * @var \TipoAlquiler
     *
     * @ORM\ManyToOne(targetEntity="TipoAlquiler")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdTipoAlquiler", referencedColumnName="IdTipoAlquiler", onDelete="CASCADE")
     * })
     */
    private $idtipoalquiler;



    /**
     * Get iditem
     *
     * @return integer
     */
    public function getIditem()
    {
        return $this->iditem;
    }

    /**
     * Set idestado
     *
     * @param \VideoclubBundle\Entity\Estado $idestado
     *
     * @return Item
     */
    public function setIdestado(\VideoclubBundle\Entity\Estado $idestado = null)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado
     *
     * @return \VideoclubBundle\Entity\Estado
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set idformato
     *
     * @param \VideoclubBundle\Entity\Formato $idformato
     *
     * @return Item
     */
    public function setIdformato(\VideoclubBundle\Entity\Formato $idformato = null)
    {
        $this->idformato = $idformato;

        return $this;
    }

    /**
     * Get idformato
     *
     * @return \VideoclubBundle\Entity\Formato
     */
    public function getIdformato()
    {
        return $this->idformato;
    }

    /**
     * Set idproducto
     *
     * @param \VideoclubBundle\Entity\Producto $idproducto
     *
     * @return Item
     */
    public function setIdproducto(\VideoclubBundle\Entity\Producto $idproducto = null)
    {
        $this->idproducto = $idproducto;

        return $this;
    }

    /**
     * Get idproducto
     *
     * @return \VideoclubBundle\Entity\Producto
     */
    public function getIdproducto()
    {
        return $this->idproducto;
    }

    /**
     * Set idtipoalquiler
     *
     * @param \VideoclubBundle\Entity\TipoAlquiler $idtipoalquiler
     *
     * @return Item
     */
    public function setIdtipoalquiler(\VideoclubBundle\Entity\TipoAlquiler $idtipoalquiler = null)
    {
        $this->idtipoalquiler = $idtipoalquiler;

        return $this;
    }

    /**
     * Get idtipoalquiler
     *
     * @return \VideoclubBundle\Entity\TipoAlquiler
     */
    public function getIdtipoalquiler()
    {
        return $this->idtipoalquiler;
    }
}
