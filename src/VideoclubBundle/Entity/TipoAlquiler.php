<?php

namespace VideoclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoAlquiler
 *
 * @ORM\Table(name="tipo_alquiler")
 * @ORM\Entity
 */
class TipoAlquiler
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdTipoAlquiler", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtipoalquiler;

    /**
     * @var integer
     *
     * @ORM\Column(name="Precio", type="integer", nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="Duracion", type="string", length=10, nullable=true)
     */
    private $duracion;

    /**
     * @var integer
     *
     * @ORM\Column(name="recargo_dia", type="integer", nullable=true)
     */
    private $recargoDia;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=45, nullable=true)
     */
    private $nombre;



    /**
     * Get idtipoalquiler
     *
     * @return integer
     */
    public function getIdtipoalquiler()
    {
        return $this->idtipoalquiler;
    }

    /**
     * Set precio
     *
     * @param integer $precio
     *
     * @return TipoAlquiler
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return integer
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set duracion
     *
     * @param string $duracion
     *
     * @return TipoAlquiler
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return string
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set recargoDia
     *
     * @param integer $recargoDia
     *
     * @return TipoAlquiler
     */
    public function setRecargoDia($recargoDia)
    {
        $this->recargoDia = $recargoDia;

        return $this;
    }

    /**
     * Get recargoDia
     *
     * @return integer
     */
    public function getRecargoDia()
    {
        return $this->recargoDia;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoAlquiler
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
