<?php

namespace VideoclubBundle\Controller;

use VideoclubBundle\Form\ProductoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use VideoclubBundle\Entity\Producto;
use Symfony\Component\HttpFoundation\Response;

class AddProductController extends Controller
{

    public function addProductAction(Request $request)
    {
      // 1) build the form
      $product = new Producto();
      $form = $this->createForm(ProductoType::class, $product);

      // 2) handle the submit (will only happen on POST)
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {

          // 4) save the Product!
          $em = $this->getDoctrine()->getManager();
          $em->persist($product);
          $em->flush();
          // ... do any other work - like sending them an email, etc
          // maybe set a "flash" success message for the user

          $this->get('session')->getFlashBag()->add(
                'notice',
                'Se ha insertado el producto.'
            );

            return $this->redirect($this->generateUrl("videoclub_showProduct"));
      }

      return $this->render('VideoclubBundle:Default:addProduct.html.twig', array('addProduct' => $form->createView()));
}
  public function viewProductAction(){
    $em = $this->getDoctrine()->getEntityManager();

    $products = $em->getRepository('VideoclubBundle:Producto')->findAll();

    return $this->render('VideoclubBundle:Default:viewProduct.html.twig', array('products' => $products));
  }
  public function deleteAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $product = $em->getRepository('VideoclubBundle:Producto')->find($id);
    $em->remove($product);
    $em->flush();
    return $this->redirectToRoute('videoclub_showProduct');
  }

  public function editAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $product = $em->getRepository('VideoclubBundle:Producto')->find($id);
    $form = $this->createForm(ProductoType::class, $product);

    // 2) handle the submit (will only happen on POST)
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        $product=$form->getData();

        // 4) save the Product!
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();
        // ... do any other work - like sending them an email, etc
        // maybe set a "flash" success message for the user

        $this->get('session')->getFlashBag()->add(
              'notice',
              'Se han guardado los cambios.'
          );

          return $this->redirect($this->generateUrl("videoclub_showProduct"));
    }

    return $this->render('VideoclubBundle:Default:addProduct.html.twig', array(
        'addProduct' => $form->createView(),
    ));
  }
}
