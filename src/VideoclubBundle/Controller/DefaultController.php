<?php

namespace VideoclubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VideoclubBundle:Default:index.html.twig');
    }

    /*
    @Route("/nombre", name = "videoclub_nombre");
    */
    //Tiene que llevar los mismos parametros que en el routing
    public function loginAction()
    {
        return $this->render('VideoclubBundle:Default:login.html.twig');
    }

    public function registerAction()
    {
        return $this->render('VideoclubBundle:Default:register.html.twig');
    }
}
