<?php

namespace VideoclubBundle\Controller;

use VideoclubBundle\Form\ClienteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VideoclubBundle\Entity\Cliente;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\HttpFoundation\Response;
use FOS\UserBundle\Event\FilterUserResponseEvent;

class AddUserController extends BaseController
{
  public function addUserAction(Request $request)
  {
      // // 1) build the form
      // $user = new Cliente();
      // $form = $this->createForm(ClienteType::class, $user);
      //
      // // 2) handle the submit (will only happen on POST)
      // $form->handleRequest($request);
      // if ($form->isSubmitted() && $form->isValid()) {
      //     $user=$form->getData();
      //
      //     // 3) Encode the password (you could also do this via Doctrine listener)
      //     $password = $this->get('security.password_encoder')
      //         ->encodePassword($user, $user->getPlainPassword());
      //     $user->setPassword($password);
      //
      //
      //     // 4) save the User!
      //     $em = $this->getDoctrine()->getManager();
      //     $em->persist($user);
      //     $em->flush();
      //     // ... do any other work - like sending them an email, etc
      //     // maybe set a "flash" success message for the user
      //
      //     return new Response('Usuario Registrado');
      // }
      //
      // return $this->render('VideoclubBundle:Default:addUser.html.twig', array('addUser' => $form->createView()));

      /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                /*****************************************************
                 * Add new functionality (e.g. log the registration) *
                 *****************************************************/
                $this->container->get('logger')->info(
                    sprintf("New user registration: %s", $user)
                );

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('videoclub_mainAdmin');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('VideoclubBundle:Default:addUser.html.twig', array(
            'addUser' => $form->createView(),
        ));
  }
  public function viewUserAction(){
    $em = $this->getDoctrine()->getEntityManager();

    $users = $em->getRepository('VideoclubBundle:Cliente')->findAll();

    return $this->render('VideoclubBundle:Default:viewUser.html.twig', array('users' => $users));
  }
  public function deleteAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('VideoclubBundle:Cliente')->find($id);
    $em->remove($user);
    $em->flush();
    return $this->redirectToRoute('videoclub_showUser');
  }

  public function editAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('VideoclubBundle:Cliente')->find($id);
    $form = $this->createForm(ClienteType::class, $user);

    // 2) handle the submit (will only happen on POST)
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        $user=$form->getData();

        // 4) save the Product!
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        // ... do any other work - like sending them an email, etc
        // maybe set a "flash" success message for the user

        $this->get('session')->getFlashBag()->add(
              'notice',
              'Se han guardado los cambios.'
          );

          return $this->redirect($this->generateUrl("videoclub_showUser"));
    }

    return $this->render('VideoclubBundle:Default:addUser.html.twig', array(
        'addUser' => $form->createView(),
    ));
  }
}
