<?php

namespace VideoclubBundle\Controller;

use VideoclubBundle\Form\AlquilarType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use VideoclubBundle\Entity\Alquiler;
use Symfony\Component\HttpFoundation\Response;
class RentController extends Controller
{

    public function rentAction(Request $request)
    {
      // 1) build the form
      $rent = new Alquiler();
      $form = $this->createForm(AlquilarType::class, $rent);

      // 2) handle the submit (will only happen on POST)
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
          $rent=$form->getData();

          // 4) save the Product!
          $em = $this->getDoctrine()->getManager();
          $em->persist($rent);
          $em->flush();
          // ... do any other work - like sending them an email, etc
          // maybe set a "flash" success message for the user

          // $url = $this->generateUrl('mainAdmin');
          // $response = new RedirectResponse($url);

          return $this->redirect($this->generateUrl("videoclub_viewRent"));
      }

      return $this->render('VideoclubBundle:Default:rent.html.twig', array('rent' => $form->createView()));
}
    public function viewRentAction(){
      $em = $this->getDoctrine()->getEntityManager();

      $rents = $em->getRepository('VideoclubBundle:Alquiler')->findAll();

      return $this->render('VideoclubBundle:Default:viewRent.html.twig', array('rents' => $rents));
    }
  public function devolAction (Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $rent = $em->getRepository('VideoclubBundle:Alquiler')->find($id);
    $form = $this->createForm(AlquilarType::class, $rent);

    // 2) handle the submit (will only happen on POST)
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        $rent=$form->getData();

        // 4) save the Product!
        $em = $this->getDoctrine()->getManager();
        $em->persist($rent);
        $em->flush();
        // ... do any other work - like sending them an email, etc
        // maybe set a "flash" success message for the user

        $this->get('session')->getFlashBag()->add(
              'notice',
              'Se han guardado los cambios.'
          );

          return $this->redirect($this->generateUrl("videoclub_viewRent"));
    }

    return $this->render('VideoclubBundle:Default:rent.html.twig', array(
        'rent' => $form->createView(),
    ));
  }
}
