<?php

namespace VideoclubBundle\Controller;

use VideoclubBundle\Form\IncidenciaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use VideoclubBundle\Entity\Incidencia;
use Symfony\Component\HttpFoundation\Response;
class IncidencesController extends Controller
{

    public function incidencesAction(Request $request)
    {
      // 1) build the form
      $incidence = new Incidencia();
      $form = $this->createForm(IncidenciaType::class, $incidence);

      // 2) handle the submit (will only happen on POST)
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
          $incidence=$form->getData();

          // 4) save the Product!
          $em = $this->getDoctrine()->getManager();
          $em->persist($incidence);
          $em->flush();
          // ... do any other work - like sending them an email, etc
          // maybe set a "flash" success message for the user
          $this->get('session')->getFlashBag()->add(
                'notice',
                'Se ha insertado la incidencia.'
            );

            return $this->redirect($this->generateUrl("videoclub_viewIncidence"));

      }

      return $this->render('VideoclubBundle:Default:incidences.html.twig', array('incidence' => $form->createView()));
}
      public function viewIncidenceAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $incidences = $em->getRepository('VideoclubBundle:Incidencia')->findAll();

        return $this->render('VideoclubBundle:Default:viewIncidence.html.twig', array('incidence' => $incidences));
      }
      public function deleteAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $incidence = $em->getRepository('VideoclubBundle:Incidencia')->find($id);
        $em->remove($incidence);
        $em->flush();
        return $this->redirectToRoute('videoclub_viewIncidence');
      }

      public function editAction(Request $request, $id)
      {
        $em = $this->getDoctrine()->getManager();
        $incidence = $em->getRepository('VideoclubBundle:Incidencia')->find($id);
        $form = $this->createForm(IncidenciaType::class, $incidence);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $incidence=$form->getData();

            // 4) save the Product!
            $em = $this->getDoctrine()->getManager();
            $em->persist($incidence);
            $em->flush();
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            $this->get('session')->getFlashBag()->add(
                  'notice',
                  'Se han guardado los cambios.'
              );

              return $this->redirect($this->generateUrl("videoclub_viewIncidence"));
        }

        return $this->render('VideoclubBundle:Default:incidences.html.twig', array(
            'incidence' => $form->createView(),
        ));
      }
}
