<?php

namespace VideoclubBundle\Controller;

use VideoclubBundle\Form\ItemType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use VideoclubBundle\Entity\Item;
use Symfony\Component\HttpFoundation\Response;

class AddItemController extends Controller
{

    public function addItemAction(Request $request)
    {
      // 1) build the form
      $item = new Item();
      $form = $this->createForm(ItemType::class, $item);

      // 2) handle the submit (will only happen on POST)
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
          $item=$form->getData();

          // 4) save the Product!
          $em = $this->getDoctrine()->getManager();
          $em->persist($item);
          $em->flush();
          // ... do any other work - like sending them an email, etc
          // maybe set a "flash" success message for the user

          return $this->redirect($this->generateUrl("videoclub_viewItem"));
      }

      return $this->render('VideoclubBundle:Default:addItem.html.twig', array('addItem' => $form->createView()));
}
    public function viewItemAction(){
      $em = $this->getDoctrine()->getEntityManager();

      $items = $em->getRepository('VideoclubBundle:Item')->findAll();

      return $this->render('VideoclubBundle:Default:viewItem.html.twig', array('items' => $items));
    }
    public function deleteAction(Request $request, $id){
      $em = $this->getDoctrine()->getManager();
      $item = $em->getRepository('VideoclubBundle:Item')->find($id);
      $em->remove($item);
      $em->flush();
      return $this->redirectToRoute('videoclub_viewItem');
    }

    public function editAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $item = $em->getRepository('VideoclubBundle:Item')->find($id);
      $form = $this->createForm(ItemType::class, $item);

      // 2) handle the submit (will only happen on POST)
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
          $item=$form->getData();

          // 4) save the Product!
          $em = $this->getDoctrine()->getManager();
          $em->persist($item);
          $em->flush();
          // ... do any other work - like sending them an email, etc
          // maybe set a "flash" success message for the user

          $this->get('session')->getFlashBag()->add(
                'notice',
                'Se han guardado los cambios.'
            );

            return $this->redirect($this->generateUrl("videoclub_viewItem"));
      }

      return $this->render('VideoclubBundle:Default:addItem.html.twig', array(
          'addItem' => $form->createView(),
      ));
    }
}
