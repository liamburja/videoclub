<?php

namespace VideoclubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MainAdminController extends Controller
{
  /**
     * @Route("/Videoclub/login", name="login")
     */
    public function MainAdminAction()
    {
      return $this->render('VideoclubBundle:Default:mainAdmin.html.twig');
    }
    public function redirectMainAction()
  {
    return $this->redirectToRoute('fos_user_security_login');
  }
}
