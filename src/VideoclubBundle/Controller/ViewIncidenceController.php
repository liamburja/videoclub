<?php

namespace VideoclubBundle\Controller;

use VideoclubBundle\Form\IncidenciaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use VideoclubBundle\Entity\Incidencia;

class ViewIncidenceController extends Controller
{
    public function viewIncidenceAction(){
      $em = $this->getDoctrine()->getEntityManager();

      $incidences = $em->getRepository('VideoclubBundle:Incidencia')->findAll();

      return $this->render('VideoclubBundle:Default:viewIncidence.html.twig', array('incidence' => $incidences));
    }
}
