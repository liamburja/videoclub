<?php

namespace VideoclubBundle\Controller;

use VideoclubBundle\Form\ReservaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use VideoclubBundle\Entity\Reserva;

class ReserveController extends Controller
{

    public function reserveAction(Request $request)
    {
      // 1) build the form
      $reserve = new Reserva();
      $form = $this->createForm(ReservaType::class, $reserve);

      // 2) handle the submit (will only happen on POST)
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
          $reserve=$form->getData();

          // 4) save the Product!
          $em = $this->getDoctrine()->getManager();
          $em->persist($reserve);
          $em->flush();
          // ... do any other work - like sending them an email, etc
          // maybe set a "flash" success message for the user

          $this->get('session')->getFlashBag()->add(
                'notice',
                'Se ha insertado la reserva.'
            );

            return $this->redirect($this->generateUrl("videoclub_viewReserve"));      }

      return $this->render('VideoclubBundle:Default:reserve.html.twig', array('reserve' => $form->createView()));
}
    public function viewReserveAction(){
      $em = $this->getDoctrine()->getEntityManager();

      $reserve = $em->getRepository('VideoclubBundle:Reserva')->findAll();

      return $this->render('VideoclubBundle:Default:viewReserve.html.twig', array('reserves' => $reserve));
    }
    public function deleteAction(Request $request, $id){
      $em = $this->getDoctrine()->getManager();
      $reserve = $em->getRepository('VideoclubBundle:Reserva')->find($id);
      $em->remove($reserve);
      $em->flush();
      return $this->redirectToRoute('videoclub_viewReserve');
    }

    public function editAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $reserve = $em->getRepository('VideoclubBundle:Reserva')->find($id);
      $form = $this->createForm(ReservaType::class, $reserve);

      // 2) handle the submit (will only happen on POST)
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
          $reserve=$form->getData();

          // 4) save the Product!
          $em = $this->getDoctrine()->getManager();
          $em->persist($reserve);
          $em->flush();
          // ... do any other work - like sending them an email, etc
          // maybe set a "flash" success message for the user

          $this->get('session')->getFlashBag()->add(
                'notice',
                'Se han guardado los cambios.'
            );

            return $this->redirect($this->generateUrl("videoclub_viewReserve"));
      }

      return $this->render('VideoclubBundle:Default:reserve.html.twig', array(
          'reserve' => $form->createView(),
      ));
    }
}
